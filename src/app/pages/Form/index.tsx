import {useParams} from 'react-router-dom';

import FormCard from '../../shared/components/FormCard';

const Form = () => {
  const params = useParams();

  return <FormCard movieId={`${params.movieId}`} />;
};

export default Form;
