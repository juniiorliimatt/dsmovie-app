import {MoviePage} from '../../types/movie';
import ArrowLeft from '../ArrowLeft';
import ArrowRight from '../ArrowRight';
import './styles.css';

type Props = {
  page: MoviePage;
  // eslint-disable-next-line @typescript-eslint/ban-types
  onChange: Function;
};

const Pagination = ({page, onChange}: Props) => {
  return (
    <div className="dsmovie-pagination-container">
      <div className="dsmovie-pagination-box">
        <button
          className="dsmovie-pagination-button"
          disabled={page.first}
          onClick={() => onChange(page.number - 1)}>
          <ArrowLeft />
        </button>
        <p>{`${page.number + 1} de ${page.totalPages}`}</p>
        <button
          className="dsmovie-pagination-button"
          disabled={page.last}
          onClick={() => onChange(page.number + 1)}>
          <ArrowRight />
        </button>
      </div>
    </div>
  );
};

export default Pagination;
