import {Route, Routes} from 'react-router-dom';

import Form from '../pages/Form';
import Listing from '../pages/Listing';

export const MyRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<Listing />} />
      <Route path="/form">
        <Route path=":movieId" element={<Form />} />
      </Route>
    </Routes>
  );
};
