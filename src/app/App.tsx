import {BrowserRouter} from 'react-router-dom';
import {MyRoutes} from './routes';
import Navbar from './shared/components/Navbar';

export const App = () => {
  return (
    <BrowserRouter>
      <Navbar />
      <MyRoutes />
    </BrowserRouter>
  );
};
